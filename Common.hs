{-|
  The @Common@ module holds functions which are used by 
  all the benchmarks.
-}
module Common where

import System.Environment
import System.CPUTime
import Control.DeepSeq
import Control.Monad
import Data.Array
  
{-|
  @groupsOfX@ is the function which generates our benchmark
  data.  It creates an array composed of two different values.  The
  values are each repeated @groupSize@ number of times, creating
  one "group" of values.  The final array is created by repeatedly
  generating groups, @numberGroups@ times.  The result is an array
  with @groupSize*2*numberGroups@ elements.  Half of them are @val@, 
  half are @other@, and they are in runs of @groupSize@.
-}
groupsOfX :: a    -- ^ The first value to put in the array
          -> a    -- ^ The second value to put in the array
          -> Int  -- ^ The number of times to repeat each value to make a "group"
          -> Int  -- ^ The number of times to repeat each group.
          -> Array Int a -- ^ The return value
groupsOfX val other groupSize numberGroups
                =   array (0,groupSize*2*numberGroups-1) 
                          (concat ( map (\x -> [(i+(x*2)*groupSize   , val)   | i <- [0 .. groupSize-1]]
                                            ++ [(i+(x*2+1)*groupSize , other) | i <- [0 .. groupSize-1]]
                                        )
                                        [0..numberGroups-1]
                                  )
                          )

{-|
  @runit@ is a function which takes a function that generates an "Int"
  and times its execution.
-}
runit :: String    -- ^ A string which identifies this test run and is used when printing the results.
      -> Int       -- ^ A function which takes no arguments and returns an integer.
      -> IO ()     -- ^ This function prints its results to the console.
runit name function = do
  -- todo: this only runs the program once. It would be nice to run it 10 or 100 times, but since Haskell keeps the result, recalculating is effectively instantinous.
  startTime <- getCPUTime
  res <- return $ function
  res `deepseq` return ()  -- force evaluation of result
  endTime <- getCPUTime
  putStr $ name ++ " :\t\t (" ++ (show res) ++ ") \t\t"
  putStr (show ((fromIntegral (endTime - startTime)) / (10^9)))
  putStr " milli secs\n"

