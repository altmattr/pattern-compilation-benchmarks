val rec listLength =
       fn n => case n of nil   => 0
                       | _::xs => 1 + listLength(xs)