listLength lst = case lst of
                 []     -> 0
                 (_:xs) -> 1+listLength(xs)