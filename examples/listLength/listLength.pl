listLength([],0).
listLength([_|xs],F) :-
    listLength(xs,F1),
    F is 1+F1.