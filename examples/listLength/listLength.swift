enum MyList<T> {
    indirect case Cons(T, MyList<T>)
    case Nil
}


func listLength<T>(lst: MyList<T>) -> Int {
  switch lst {
    case let MyList.Cons(_,xs):
      return 1+listLength(lst:xs)
    case MyList.Nil:
      return 0
  }
}
