import Html
listLength : List a -> Int
listLength lst =
  case lst of
    [] -> 0
    x::xs -> 1+listLength(xs)

main =
  Html.text (String.fromInt(listLength([1,2,3,4,5])))