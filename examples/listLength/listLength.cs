using System;
public class Program{
	public static void Main(){	
		var test = new MyList();
		test.Add(0);
		test.Add(1);
		test.Add(2);
		test.Add(3);
		Console.WriteLine(listLength(test));
	}
	
	public static int listLength(MyList lst){
		switch(lst.head)
		{
			case Nil n:
				return 0;
			case Cons c:
				lst.Remove();
				return 1+listLength(lst);
			default:
				return 0;
		}
	}
}

public class MyList{
	public MyListNode head;
	public MyList(){
		head = new Nil();
	}
	public MyList(MyListNode n){
		head = n;	
	}
	
	public Nullable<int> Read(){
		return head.v;
	}
	public void Add(int val){
		head = new Cons(head,val);
	}
	public void Remove(){
		switch(head){
			case Cons h:
				head = h.n;
				return;
			case Nil h:
				return;
		}
	}
}

public abstract class MyListNode {
	public Nullable<int> v;
}

public class Cons : MyListNode{
	public MyListNode n;
	
	public Cons(MyListNode next, int val){
		n = next;
		v = val;
	}
	

}

public class Nil : MyListNode{
	public Nil(){
		v = null;
	}
}





