def listLength[T](list: Seq[T]): Int = list match {
 case Nil => 0
 case _ :: rest => 1+listLength(rest)
}