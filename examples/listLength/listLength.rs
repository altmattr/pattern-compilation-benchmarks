fn listLength(lst: &[i32]) -> i32{
    match lst{
        [] => return 0,
        x  => return 1+listLength(&x[1..])
    }
}