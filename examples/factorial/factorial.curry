factorial :: Prelude.Int -> Prelude.Int
factorial n = case n of
          0 -> 1
          x -> x*factorial(x-1)

main = factorial(5)