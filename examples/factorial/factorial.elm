factorial : Int -> Int
factorial n =
  case n of
    0 -> 1
    _ -> n*factorial(n-1)
