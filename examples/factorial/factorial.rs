fn factorial(n : i32) -> i32{
    match n {
        0 => return 1,
        x => return factorial(x-1)*x
    }
}