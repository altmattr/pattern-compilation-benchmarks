func factorial(n: Int) -> Int {
  switch n {
    case 0:
      return 1
    case let x:
      return x*factorial(n:(x-1))
  }
}