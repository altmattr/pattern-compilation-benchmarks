factorial :: Integer -> Integer
factorial n = case n of
          0 -> 1
          x -> x*factorial(x-1)