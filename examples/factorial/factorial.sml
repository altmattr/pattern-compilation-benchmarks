val rec factorial =
       fn n => case n of 0 => 1
                       | n => n * factorial (n - 1)