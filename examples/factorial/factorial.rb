def factorial(n)
  case n
  in 0
    1
  in x
    x*factorial(x-1)
  end
end