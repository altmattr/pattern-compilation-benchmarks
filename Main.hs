import Criterion.Main
import BenchmarkA
import Common

main = defaultMain [bench "booleans" $ nf (booleanRun 0 0 0) (groupsOfX True False size size) | size <- [0..100]]
