/**
  BenchmarkA compares a drop-down discrimination mechanism with pattern
  matching (which we expect will be compiled to a jump mechanism).  It
  does so for a very small data type, with just two constructors.  This 
  allows a comparison of the two different mechanism while minimising the
  effect of compiling to a jump mechanism.  The two discrimination 
  mechanisms we compare are `if` expressions and pattern matching.  If
  expressions work on booleans and so don't need a data type definition. 
  To compare this to pattern matching we need an equivalent algebraic data
  type, which we call "On"
**/
object BenchmarkA{
  import Common._
  
  /** An algebraic data type equivalent to booleans **/
  abstract class State()
    case class On() extends State()
    case class Off() extends State()
    
  /**
    Calculate the longest run of `true` values in an array of
    booleans.  The algorithm uses recursion to iterate through
    the array.
  **/
  def booleanRun(data: Array[Boolean]): Int = {
    var maxRun = 0
    var run = 0
    for(i <- 0 until data.length){
      if (data(i)){
        run = run + 1
      }
      else{
        maxRun = scala.math.max(run, maxRun)
        run = 0
      }
    }
    maxRun
  }
  
  /**
    Calculate the longest run of @On@ values in an array of
    states.  The algorithm uses recursion to iterate through
    the array.
  **/
  def stateRun(data: Array[State]): Int = {
    var maxRun = 0
    var run = 0
    for(i <- 0 until data.length){
      data(i) match {
        case On()  => run = run + 1
        case Off() => { maxRun = scala.math.max(run, maxRun)
                        run = 0
                      }
      }
    }
    maxRun
  }

  /**
    Compare the run-time of "booleanRun" and "stateRun" on arrays
    with equivalent values.  Program must be run with two arguments
    giving the group size and number of groups for the arrays which
    the benchmark is run for.
  **/
  def main(args: Array[String]) {
    val groupSize = args(0).toInt
    val numGroups = args(1).toInt
    val booleanData = groupsOfX[Boolean](true , false  , groupSize, numGroups)
    val stateData   = groupsOfX[State]  (On() , Off()  , groupSize, numGroups)
    runit("bool" , booleanRun(booleanData))
    runit("state", stateRun(stateData))
  }
}