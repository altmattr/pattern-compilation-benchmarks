{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_pattern_compilation_benchmarks (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/overflow/repos/pattern-compilation-benchmarks/.stack-work/install/x86_64-linux/nightly-2019-02-09/8.6.3/bin"
libdir     = "/overflow/repos/pattern-compilation-benchmarks/.stack-work/install/x86_64-linux/nightly-2019-02-09/8.6.3/lib/x86_64-linux-ghc-8.6.3/pattern-compilation-benchmarks-0.1.0.0-8GFdAOeoMHLAWWsEGwztQg-site"
dynlibdir  = "/overflow/repos/pattern-compilation-benchmarks/.stack-work/install/x86_64-linux/nightly-2019-02-09/8.6.3/lib/x86_64-linux-ghc-8.6.3"
datadir    = "/overflow/repos/pattern-compilation-benchmarks/.stack-work/install/x86_64-linux/nightly-2019-02-09/8.6.3/share/x86_64-linux-ghc-8.6.3/pattern-compilation-benchmarks-0.1.0.0"
libexecdir = "/overflow/repos/pattern-compilation-benchmarks/.stack-work/install/x86_64-linux/nightly-2019-02-09/8.6.3/libexec/x86_64-linux-ghc-8.6.3/pattern-compilation-benchmarks-0.1.0.0"
sysconfdir = "/overflow/repos/pattern-compilation-benchmarks/.stack-work/install/x86_64-linux/nightly-2019-02-09/8.6.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "pattern_compilation_benchmarks_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "pattern_compilation_benchmarks_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "pattern_compilation_benchmarks_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "pattern_compilation_benchmarks_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "pattern_compilation_benchmarks_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "pattern_compilation_benchmarks_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
