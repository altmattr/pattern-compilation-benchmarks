{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE PatternGuards #-}

module Wadler87 where

import Data.Default (def)
import AutoBench.Types
import Test.QuickCheck

analysisOpts :: AnalOpts
analysisOpts = def {_linearModels=[Poly 1, Poly 4, Exp 2]}

basicBench :: TestSuite
basicBench = def {_analOpts = analysisOpts, _dataOpts = Manual "pileOfInts"}

-- * Integers as Peano

-- ** View Patterns
data Peano = Zero | Succ Peano

peano :: Int -> Peano
peano 0 = Zero
peano x = Succ (peano (x - 1))

unpeano :: Peano -> Int
unpeano Zero = 0
unpeano (Succ x) = 1 + (unpeano x)

multByAdding :: Int -> Int -> Int
multByAdding x b | x < 0 = 0
multByAdding 0 b = 0
multByAdding 1 b = b
multByAdding a b = b + multByAdding (a - 1) b

prop_mult :: Int -> Int -> Property
prop_mult a b = (a > 0) ==> (multByAdding a b === (a * b))

squareByPattern:: Int -> Int
squareByPattern a = multByAdding a a

multByAddingPeano :: Peano -> Peano -> Peano
multByAddingPeano Zero b = Zero
multByAddingPeano (Succ Zero) b = b
multByAddingPeano (Succ a) b = peanoPlus b (multByAddingPeano a b)
  where peanoPlus Zero b = b
        peanoPlus (Succ a) b = Succ (peanoPlus a b)

prop_peano :: Int -> Int -> Property
prop_peano a b = (a > 0) ==> multByAdding a b === unpeano (multByAddingPeano (peano a) (peano b))

squareByPeano :: Int -> Int
squareByPeano a = let pa = peano a
                  in  unpeano (multByAddingPeano pa pa)

benchmarkNormalAgainstPeano :: TestSuite
benchmarkNormalAgainstPeano = basicBench {_progs = ["squareByPattern", "squareByPeano"]}

-- | Note that we are not interested in adding overhead due to the peano arithmetic itself!  Just the view.
multByAddingView :: Int -> Int -> Int
multByAddingView x b | x < 0 = 0
multByAddingView (peano -> Zero) b = 0
multByAddingView (peano -> Succ Zero) b = b
multByAddingView (peano -> Succ a') b = b + (multByAddingView (unpeano a') b)

prop_view :: Int -> Int -> Property
prop_view a b = multByAdding a b === multByAddingView a b

squareByView :: Int -> Int
squareByView a = multByAddingView a a

pileOfInts :: UnaryTestData Int
pileOfInts = [(s, return s) | s <- [0,50..1000]]

benchmarkNormalAgainstPeanoAgainstView :: TestSuite
benchmarkNormalAgainstPeanoAgainstView = basicBench {_progs = ["squareByPeano", "squareByView", "squareByPattern"]}

-- ** Pattern Guards

multByAddingGuard :: Int -> Int -> Int
multByAddingGuard x b | x < 0 = 0
multByAddingGuard a b | Zero <- peano a = 0
multByAddingGuard a b | Succ Zero <- peano a = b
multByAddingGuard a b | Succ a' <- peano a = b + (multByAddingGuard (unpeano a') b)

prop_guard :: Int -> Int -> Property
prop_guard a b = multByAdding a b === multByAddingGuard a b

squareByGuard :: Int -> Int
squareByGuard a = multByAddingGuard a a

benchmarkNormalAgainstPeanoAgainstGuards :: TestSuite
benchmarkNormalAgainstPeanoAgainstGuards = basicBench {_progs = ["squareByPeano", "squareByGuard", "squareByPattern"]}

-- ** Pattern Synonyms

-- need a helper view pattern!
plusOne x = x - 1
pattern SynZero = 0
pattern SynSucc a <- (plusOne -> a)

multByAddingSyn :: Int -> Int -> Int
multByAddingSyn a b | a < 0 = 0
multByAddingSyn SynZero b = 0
multByAddingSyn (SynSucc SynZero) b = b
multByAddingSyn (SynSucc a') b  = b + multByAddingSyn a' b

prop_syn :: Int -> Int -> Property
prop_syn a b = multByAdding a b === multByAddingSyn a b

squareBySyn :: Int -> Int
squareBySyn a = multByAddingSyn a a

benchmarkNormalAgainstPeanoAgainstSyn :: TestSuite
benchmarkNormalAgainstPeanoAgainstSyn = basicBench {_progs = ["squareByPeano", "squareBySyn", "squareByPattern"]}

benchmarkNormalAgainstSyn :: TestSuite
benchmarkNormalAgainstSyn = basicBench {_progs = ["squareByPattern", "squareBySyn"]}
