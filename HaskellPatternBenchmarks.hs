{-# LANGUAGE PatternSynonyms #-}

module HaskellPatternBenchmarks where

import Data.Default (def)
import AutoBench.Types
import Debug.Trace
import Data.Array
import Prelude hiding (Num)

{-|
  @groupsOfX@ is the function which generates our benchmark
  data.  It creates an array composed of two different values.  The
  values are each repeated @groupSize@ number of times, creating
  one "group" of values.  The final array is created by repeatedly
  generating groups, @numberGroups@ times.  The result is an array
  with @groupSize*2*numberGroups@ elements.  Half of them are @val@, 
  half are @other@, and they are in runs of @groupSize@.
-}
groupsOfX :: a    -- ^ The first value to put in the array
          -> a    -- ^ The second value to put in the array
          -> Int  -- ^ The number of times to repeat each value to make a "group"
          -> Int  -- ^ The number of times to repeat each group.
          -> Array Int a -- ^ The return value
groupsOfX val other groupSize numberGroups
                =   array (0,groupSize*2*numberGroups-1) 
                          (concat ( map (\x -> [(i+(x*2)*groupSize   , val)   | i <- [0 .. groupSize-1]]
                                            ++ [(i+(x*2+1)*groupSize , other) | i <- [0 .. groupSize-1]]
                                        )
                                        [0..numberGroups-1]
                                  )
                          )

-- |An algebraic data type equivalent to booleans
data State = On 
           | Off 
           deriving Show

-- | A Pattern Synonym for State
pattern POn = On
pattern POff = Off

{-|
  Calculate the longest run of @true@ values in an array of
  booleans.  The algorithm uses recursion to iterate through
  the array.
-}
booleanRun :: Int   -- ^ The maximim run so far seen
           -> Int   -- ^ The length of the run we are currently in
           -> Int   -- ^ The array element we are inspecting on this call
           -> Array Int Bool -- ^ The array we are inspecting.
           -> Int   -- ^ The longest run in the array @d@
booleanRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else if d!i then booleanRun maxRun (currRun+1) (i+1) d
                                               else booleanRun (max maxRun currRun) 0 (i+1) d

{-|
  Calculate the longest run of @On@ values in an array of
  states.  The algorithm uses recursion to iterate through
  the array.
-}
stateRun :: Int    -- ^ The maximum run so far seen
         -> Int    -- ^ The length of the run we are currently in
         -> Int    -- ^ The array element we are inspecting on this call
         -> Array Int State -- ^ The array we are inspecting.
         -> Int    -- ^ The longest run in the array @d@
stateRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          On  -> stateRun maxRun (currRun+1) (i+1) d
                                          Off -> stateRun (max maxRun currRun) 0 (i+1) d

{-|
  Calculate the longest run of @On@ values in an array of
  states.  The algorithm uses recursion to iterate through
  the array but uses the pattern synonym for pattern matching.
-}
stateRunSyn maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          POn  -> stateRunSyn maxRun (currRun+1) (i+1) d
                                          POff -> stateRunSyn (max maxRun currRun) 0 (i+1) d

boolBySize :: Int -> Int
boolBySize size = booleanRun 0 0 0 (groupsOfX True False size size)
stateBySize :: Int -> Int
stateBySize size = stateRun 0 0 0 (groupsOfX On Off size size)
stateSynBySize :: Int -> Int
stateSynBySize size = stateRunSyn 0 0 0 (groupsOfX On Off size size)

-- |An algebraic data type equivalent to the first ten integers.
data Num   = One 
           | Two
           | Three
           | Four
           | Five
           | Six
           | Seven
           | Eight
           | Nine
           | Ten
           | Zero
           | Eleven
           | Twelve
           | Thirteen
           | Fourteen
           | Fifteen
           | Sixteen
           | Seventeen
           | Eighteen
           | Nineteen
           deriving Show

numFromInt :: Int -> Num
numFromInt 0 = Zero
numFromInt 1 = One
numFromInt 2 = Two
numFromInt 3 = Three
numFromInt 4 = Four
numFromInt 5 = Five
numFromInt 6 = Six
numFromInt 7 = Seven
numFromInt 8 = Eight
numFromInt 9 = Nine
numFromInt 10 = Ten
numFromInt 11 = Eleven
numFromInt 12 = Twelve
numFromInt 13 = Thirteen
numFromInt 14 = Fourteen
numFromInt 15 = Fifteen
numFromInt 16 = Sixteen
numFromInt 17 = Seventeen
numFromInt 18 = Eighteen
numFromInt 19 = Nineteen

pattern POne = One
pattern PTwo = Two
pattern PThree = Three
pattern PFour = Four
pattern PFive = Five
pattern PSix = Six
pattern PSeven = Seven
pattern PEight = Eight
pattern PNine = Nine
pattern PTen = Ten
pattern PZero = Zero
pattern PEleven = Eleven
pattern PTwelve = Twelve
pattern PThirteen = Thirteen
pattern PFourteen = Fourteen
pattern PFifteen = Fifteen
pattern PSixteen = Sixteen
pattern PSeventeen = Seventeen
pattern PEighteen = Eighteen
pattern PNineteen = Nineteen

{-|
  Calculate the sum of the longest run of non-zero values in an array of
  integers.  The algorithm uses recursion to iterate through
  the array.
-}
integerRun :: Int    -- ^ The maximim run sum so far seen
           -> Int    -- ^ The sum of the run we are currently in
           -> Int    -- ^ The array element we are inspecting on this call
           -> Array Int Int  -- ^ The array we are inspecting.
           -> Int    -- ^ The sum of the longest run in the array @d@
integerRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else if( d!i == 1) 
                                        then integerRun maxRun (currRun+1) (i+1) d
                                        else if (d!i == 2) 
                                             then integerRun maxRun (currRun+2)  (i+1) d
                                             else if (d!i == 3) 
                                                  then integerRun maxRun (currRun+3)  (i+1) d
                                                  else if (d!i == 4) 
                                                       then integerRun maxRun (currRun+4)  (i+1) d
                                                       else if (d!i == 5) 
                                                            then integerRun maxRun (currRun+5)  (i+1) d
                                                            else if (d!i == 6) 
                                                                 then integerRun maxRun (currRun+6)  (i+1) d
                                                                  else if (d!i == 7) 
                                                                       then integerRun maxRun (currRun+7)  (i+1) d
                                                                       else if (d!i == 8) 
                                                                            then integerRun maxRun (currRun+8)  (i+1) d
                                                                            else if (d!i == 9) 
                                                                                 then integerRun maxRun (currRun+9)  (i+1) d
                                                                                 else if (d!i == 10)
                                                                                      then integerRun maxRun (currRun+10) (i+1) d
                                                                                      else if (d!i == 11)
                                                                                        then integerRun maxRun (currRun+11) (i+1) d
                                                                                        else if (d!i == 12)
                                                                                          then integerRun maxRun (currRun+12) (i+1) d
                                                                                          else if (d!i == 13)
                                                                                            then integerRun maxRun (currRun+13) (i+1) d
                                                                                            else if (d!i == 14)
                                                                                              then integerRun maxRun (currRun+14) (i+1) d
                                                                                              else if (d!i == 15)
                                                                                                then integerRun maxRun (currRun+15) (i+1) d
                                                                                                else if (d!i == 16)
                                                                                                  then integerRun maxRun (currRun+16) (i+1) d
                                                                                                  else if (d!i == 17)
                                                                                                    then integerRun maxRun (currRun+17) (i+1) d
                                                                                                    else if (d!i == 18)
                                                                                                      then integerRun maxRun (currRun+18) (i+1) d
                                                                                                      else if (d!i == 19)
                                                                                                        then integerRun maxRun (currRun+19) (i+1) d
                                                                                                        else integerRun (max maxRun currRun) 0 (i+1) d
{-|
  Calculate the sum of the longest run of non-zero values in an array of
  "Num"s.  The algorithm uses recursion to iterate through
  the array.
-}
numRun :: Int    -- ^ The maximim run sum so far seen
       -> Int    -- ^ The sum of the run we are currently in
       -> Int    -- ^ The array element we are inspecting on this call
       -> Array Int Num   -- ^ The array we are inspecting.
       -> Int    -- ^ The sum of the longest run in the array @d@
numRun maxRun currRun i d     = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          One   -> numRun maxRun (currRun+1) (i+1) d
                                          Two   -> numRun maxRun (currRun+2) (i+1) d
                                          Three -> numRun maxRun (currRun+3) (i+1) d
                                          Four  -> numRun maxRun (currRun+4) (i+1) d
                                          Five  -> numRun maxRun (currRun+5) (i+1) d
                                          Six   -> numRun maxRun (currRun+6) (i+1) d
                                          Seven -> numRun maxRun (currRun+7) (i+1) d
                                          Eight -> numRun maxRun (currRun+8) (i+1) d
                                          Nine  -> numRun maxRun (currRun+9) (i+1) d
                                          Ten   -> numRun maxRun (currRun+10) (i+1) d
                                          Eleven    -> numRun maxRun (currRun+11) (i+1) d
                                          Twelve    -> numRun maxRun (currRun+12) (i+1) d
                                          Thirteen  -> numRun maxRun (currRun+13) (i+1) d
                                          Fourteen  -> numRun maxRun (currRun+14) (i+1) d
                                          Fifteen   -> numRun maxRun (currRun+15) (i+1) d
                                          Sixteen   -> numRun maxRun (currRun+16) (i+1) d
                                          Seventeen -> numRun maxRun (currRun+17) (i+1) d
                                          Eighteen  -> numRun maxRun (currRun+18) (i+1) d
                                          Nineteen  -> numRun maxRun (currRun+19) (i+1) d
                                          Zero  -> numRun (max maxRun currRun) 0 (i+1) d

numRunSyn maxRun currRun i d     = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          POne   -> numRunSyn maxRun (currRun+1) (i+1) d
                                          PTwo   -> numRunSyn maxRun (currRun+2) (i+1) d
                                          PThree -> numRunSyn maxRun (currRun+3) (i+1) d
                                          PFour  -> numRunSyn maxRun (currRun+4) (i+1) d
                                          PFive  -> numRunSyn maxRun (currRun+5) (i+1) d
                                          PSix   -> numRunSyn maxRun (currRun+6) (i+1) d
                                          PSeven -> numRunSyn maxRun (currRun+7) (i+1) d
                                          PEight -> numRunSyn maxRun (currRun+8) (i+1) d
                                          PNine  -> numRunSyn maxRun (currRun+9) (i+1) d
                                          PTen   -> numRunSyn maxRun (currRun+10) (i+1) d
                                          PEleven    -> numRun maxRun (currRun+11) (i+1) d
                                          PTwelve    -> numRun maxRun (currRun+12) (i+1) d
                                          PThirteen  -> numRun maxRun (currRun+13) (i+1) d
                                          PFourteen  -> numRun maxRun (currRun+14) (i+1) d
                                          PFifteen   -> numRun maxRun (currRun+15) (i+1) d
                                          PSixteen   -> numRun maxRun (currRun+16) (i+1) d
                                          PSeventeen -> numRun maxRun (currRun+17) (i+1) d
                                          PEighteen  -> numRun maxRun (currRun+18) (i+1) d
                                          PNineteen  -> numRun maxRun (currRun+19) (i+1) d
                                          PZero  -> numRunSyn (max maxRun currRun) 0 (i+1) d


groupSize = 100
numGroups = 100
integerByRank :: Int -> Int
integerByRank rank = integerRun 0 0 0 (groupsOfX rank 0 groupSize numGroups)
numByRank :: Int -> Int
numByRank rank = numRun 0 0 0 (groupsOfX (numFromInt rank) Zero groupSize numGroups)
numSynByRank :: Int -> Int
numSynByRank rank = numRunSyn 0 0 0 (groupsOfX (numFromInt rank) Zero groupSize numGroups)

zeroToOneHundred :: UnaryTestData Int
zeroToOneHundred = [(s, return s) | s <- [0,5..100]]

zeroToTen :: UnaryTestData Int
zeroToTen = [(s, return s) | s <- [0..19]]

booleanBenchmark :: TestSuite
booleanBenchmark = def {_progs = ["boolBySize", "stateBySize", "stateSynBySize"], _dataOpts = Manual "zeroToOneHundred"} 

numberBenchmark :: TestSuite
numberBenchmark = def {_progs = ["integerByRank", "numByRank", "numSynByRank"], _dataOpts = Manual "zeroToTen"} 
