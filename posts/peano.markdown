---
title: Peano Arithmetic with Patterns
author: Matt
published: 2019-02-09
---

The very first example [Wadler uses to motivate view patterns](https://www.cs.tufts.edu/~nr/cs257/archive/phil-wadler/views.pdf) is Peano Arithmetic.  Wadler explains that the Peano encoding of numbers is better for analysis but inneficient in practice.  In this article I explore ways of using pattern views (and related features) to work with Peano Arithmetic in Haskell with a particular eye to the run-time behaviour of the various encoding.

[The full source is available at bitbucket](https://bitbucket.org/altmattr/pattern-compilation-benchmarks/src/master/Wadler87.hs).

We use [AutoBench](https://github.com/mathandley/AutoBench) to look after benchmarking and to create the graphs you see below.

Our test program is one that will multiply two numbers but by recursively adding the first number over and over (`multByAdding`) and a single-argument function that uses that function to calculate the square of its input (`squareByPattern`)

~~~
multByAdding :: Int -> Int -> Int
multByAdding x b | x < 0 = 0
multByAdding 0 b = 0
multByAdding 1 b = b
multByAdding a b = b + multByAdding (a - 1) b

squareByPattern :: Int -> Int
squareByPattern a = multByAdding a a
~~~

We define a peano version of this as well (`multByAddingPeano`), lets compare the performance of each.

<img alt="normal vs peano" src="/images/normalAgainstPeano.png" width="1000"/>

Unsurprisingly, the peano version is far slower.  However, this does provide two limits we might expect our pattern variants to sit between.  The fastest we can expect is the standard algorithm and the slowest we might expect is one that does everything "the peano way".

# Pattern views

Pattern views require a conversion in-to (`peano`) and out-of (`unpeano`) the peano data type (`Peano`).  The function implmenting multiplication by addition via views is called `multByAdditionView` and the associated squareing function is `squareByView`.

You can see it splits the difference between the two but crucially has a rising cost-curve (like peano) where the standard pattern cost is linear (you will have to trust me, you can't see it down there).

<img alt="peano vs normal vs views" src="/images/normalAgainstPeanoAgainstView.png" width="1000"/>

# Pattern Guards

`multByAddingGuard`/`squareByGuard` is another variant, this time using pattern guards instead of view patterns.  We would expect to see a repeat of what we got for views, and indeed we do.

<img alt="peano vs normal vs guards" src="/images/normalAgainstPeanoAgainstGuard.png" width="1000"/>

# Pattern Synonyms

Pattern synonyms give the most interesting results.  Pattern synonyms can be almost cost-free but, as you can see in the definitions of `PZero` and `PSucc`, we have do do some computation in the conversion.  We have created another view pattern, this time viewing an integer as the integer below it and used it in the pattern synonym for `PSucc`.  Regardless, the performance for the pattern synonym version (`multByAddingSyn`/`squareBySyn`) is significantly better than the other variants.

<img alt="peano vs normal vs synonyms" src="/images/normalAgainstPeanoAgainstSyn.png" width="1000"/>

In fact, it grows linearly, as we can see if we plot it only against the normal pattern matching implementation.  Note the change in scale on the y-axis from milli-seconds to micro-seconds!

<img alt="normal vs synonyms" src="/images/normalAgainstSyn.png" width="1000"/>

# Conclusion

   * Pattern synonyms allow us to use Peano encoding of integers _without all of the overhead that normally comes with that encoding_.  
   * Pattern synonyms _in this case_ have a _constant cost_.


