{-|
  BenchmarkA compares a drop-down discrimination mechanism with pattern
  matching (which we expect will be compiled to a jump mechanism).  It
  does so for a very small data type, with just two constructors.  This 
  allows a comparison of the two different mechanism while minimising the
  effect of compiling to a jump mechanism.  The two discrimination 
  mechanisms we compare are @if@ expressions and pattern matching.  If
  expressions work on booleans and so don't need a data type definition. 
  To compare this to pattern matching we need an equivalent algebraic data
  type, which we call "State" with constructors `On` and `Off`.  To compare to
  the raw overhead of pattern synonyms, we set up synonyms for `On` and `Off`,
  `POn` and `POff` respectively.
-}

{-# LANGUAGE PatternSynonyms #-}
module BenchmarkA ( State(.., POn, POff)
                  , booleanRun
                  , stateRun
                  , synRun
                  )
where

import Common
import System.Environment
import Data.Array

-- |An algebraic data type equivalent to booleans
data State = On 
           | Off 
           deriving Show

-- | A Pattern Synonym for State
pattern POn = On
pattern POff = Off

{-|
  Calculate the longest run of @true@ values in an array of
  booleans.  The algorithm uses recursion to iterate through
  the array.
-}
booleanRun :: Int   -- ^ The maximim run so far seen
           -> Int   -- ^ The length of the run we are currently in
           -> Int   -- ^ The array element we are inspecting on this call
           -> Array Int Bool -- ^ The array we are inspecting.
           -> Int   -- ^ The longest run in the array @d@
booleanRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else if d!i then booleanRun maxRun (currRun+1) (i+1) d
                                               else booleanRun (max maxRun currRun) 0 (i+1) d

{-|
  Calculate the longest run of @On@ values in an array of
  states.  The algorithm uses recursion to iterate through
  the array.
-}
stateRun :: Int    -- ^ The maximum run so far seen
         -> Int    -- ^ The length of the run we are currently in
         -> Int    -- ^ The array element we are inspecting on this call
         -> Array Int State -- ^ The array we are inspecting.
         -> Int    -- ^ The longest run in the array @d@
stateRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          On  -> stateRun maxRun (currRun+1) (i+1) d
                                          Off -> stateRun (max maxRun currRun) 0 (i+1) d

{-|
  Calculate the longest run of @On@ values in an array of
  states.  The algorithm uses recursion to iterate through
  the array but uses the pattern synonym for pattern matching.
-}
synRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          POn  -> synRun maxRun (currRun+1) (i+1) d
                                          POff -> synRun (max maxRun currRun) 0 (i+1) d

{-|
  Compare the run-time of "booleanRun" and "stateRun" on arrays
  with equivalent values.  Program must be run with two arguments
  giving the group size and number of groups for the arrays which
  the benchmark is run for.
-}
main :: IO()
main = do
  args <- getArgs
  let groupSize = read (args!!0) :: Int
  let numGroups = read (args!!1) :: Int
  let booleanData = groupsOfX True False groupSize numGroups
  let stateData   = groupsOfX On   Off   groupSize numGroups
  -- force evaluation of each
  writeFile "/dev/null" (show booleanData)
  writeFile "/dev/null" (show stateData)
  -- run benchmarks
  runit "bool"  $ booleanRun 0 0 0 booleanData
  runit "state" $ stateRun 0 0 0 stateData
  runit "syn"   $ synRun 0 0 0 stateData
