/**
  The `Common` module holds functions which are used by 
  all the benchmarks.
**/
object Common{
  import java.lang.management._
  val bean = ManagementFactory.getThreadMXBean();
  

  /**
    `groupsOfX` is the function which generates our benchmark
    data.  It creates an array composed of two different values.  The
    values are each repeated `groupSize` number of times, creating
    one "group" of values.  The final array is created by repeatedly
    generating groups, `numberGroups` times.  The result is an array
    with `groupSize*3*numberGroups` elements.  Half of them are `val`, 
    half are `other`, and they are in runs of `groupSize`.
  **/
  def groupsOfX[A: Manifest](v: A, other: A, groupSize: Int, numberGroups: Int): Array[A] = {
    val list: List[A] 
            = (List.range(0,numberGroups)).flatMap({x => 
                val vs     = (for (i <- List.range(0, groupSize)) yield v)
                val others = (for (i <- List.range(0, groupSize)) yield other)
                vs ++ others
              })
    list.toArray[A]
  }
  
  /**
    `runit` is a function which takes a function that generates an `Int`
    and times its execution.
  **/
  def runit(name: String, function: => Int): Unit = {
    val startTime = bean.getCurrentThreadCpuTime()
    var i = 0;
    var res = 0;
    while (i < 100){
      res = function
      i = i + 1
    }
    val endTime = bean.getCurrentThreadCpuTime()
    println(name ++ ":\t\t (" ++ res.toString() ++ ") \t\t" ++ ((endTime - startTime) / (1000000)).toString() ++ " milli secs")
  }
}