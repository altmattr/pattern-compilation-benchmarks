/**
  BenchmarkB compares a drop-down discrimination mechanism with pattern
  matching (which we expect will be compiled to a jump mechanism).  It
  does so for a very large data type, with eleven constructors.  This 
  allows a comparison of the two different mechanism exposing the
  effect of compiling to a jump mechanism.  The two discrimination 
  mechanisms we compare are `if` expressions and pattern matching.  If
  expressions work using an equality check on integers so don't need a 
  data type definition.  To compare this to pattern matching we need an 
  equivalent algebraic data type, which we call "Num".  "Num" only includes
  the first ten integers because that is enough to expose the behaviour we
  are trying to capture.
**/
object BenchmarkB{
  import Common._
  
  /** An algebraic data type equivalent to the first ten integers. **/
  abstract class Num()
    case class One()   extends Num()
    case class Two()   extends Num()
    case class Three() extends Num()
    case class Four()  extends Num()
    case class Five()  extends Num()
    case class Six()   extends Num()
    case class Seven() extends Num()
    case class Eight() extends Num()
    case class Nine()  extends Num()
    case class Ten()   extends Num()
    case class Zero() extends Num()

  /**
    Calculate the sum of the longest run of non-zero values in an array of
    integers.  The algorithm uses recursion to iterate through
    the array.
  **/
  def integerRun(data: Array[Int]): Int = {
    var maxRun = 0
    var run = 0
    for(i <- 0 until data.length){
      if     (data(i) == 1){run = run + 1}
      else if(data(i) == 2){run = run + 2}
      else if(data(i) == 3){run = run + 3}
      else if(data(i) == 4){run = run + 4}
      else if(data(i) == 5){run = run + 5}
      else if(data(i) == 6){run = run + 6}
      else if(data(i) == 7){run = run + 7}
      else if(data(i) == 8){run = run + 8}
      else if(data(i) == 9){run = run + 9}
      else if(data(i) == 10){run = run + 10}
      else {
        maxRun = scala.math.max(run, maxRun)
        run = 0
      }
    }
    maxRun
  }

  /**
    Calculate the sum of the longest run of non-zero values in an array of
    "Num"s.  The algorithm uses recursion to iterate through
    the array.
  **/
  def numRun(data: Array[Num]): Int = {
    var maxRun = 0
    var run = 0
    for(i <- 0 until data.length){
      data(i) match {
        case One()   => run = run + 1
        case Two()   => run = run + 2
        case Three() => run = run + 3
        case Four()  => run = run + 4
        case Five()  => run = run + 5
        case Six()   => run = run + 6
        case Seven() => run = run + 7
        case Eight() => run = run + 8
        case Nine()  => run = run + 9
        case Ten()   => run = run + 10
        case _       => { maxRun = scala.math.max(run, maxRun)
                          run = 0
                        }
      }
    }
    maxRun
  }

  /**
    Compare the run-time of "booleanRun" and "stateRun" on three sets of
    arrays with equivalent values.  One run is with an array of ones and zeros, which
    should show only a little difference between `if` and `match`.  The second and third runs
    use arrays which are half zero and half 5 and 10 respectively.  We expect
    that (since `match` is not compiled to jump) that in all three cases the time for `match`
    will be similar to the time for `if`.
    This program must be run with two arguments giving the group size and 
    number of groups for the arrays which the benchmark is run for.
  **/
  def main(args: Array[String]) {
    val groupSize = args(0).toInt
    val numGroups = args(1).toInt
    val nums = Array(Zero(), One(), Two(), Three(), Four(), Five(), Six(), Seven(), Eight(), Nine(), Ten())
    for (i <- List.range(1,11)){
      val integerData = groupsOfX[Int](i, 0, groupSize, numGroups)
      runit("int-"+i, integerRun(integerData))
      val numData = groupsOfX[Num](nums(i), Zero(), groupSize, numGroups)
      runit("num-"+i, numRun(numData))
    }
    val integerDataOne = groupsOfX[Int]    (1    , 0      , groupSize, numGroups)
    val numDataOne     = groupsOfX[Num]    (One(), Zero(), groupSize, numGroups)
    val integerDataFive= groupsOfX[Int]    (5    , 0      , groupSize, numGroups)
    val numDataFive    = groupsOfX[Num]    (Five(), Zero(), groupSize, numGroups)
    val integerDataTen = groupsOfX[Int]    (10   , 0      , groupSize, numGroups)
    val numDataTen     = groupsOfX[Num]    (Ten(), Zero(), groupSize, numGroups)
    runit("alt int-1"  , integerRun(integerDataOne))
    runit("alt num-1"  , numRun(numDataOne))
    runit("alt int-5"  , integerRun(integerDataFive))
    runit("alt num-5"  , numRun(numDataFive))
    runit("alt int-10" , integerRun(integerDataTen))
    runit("alt num-10" , numRun(numDataTen))
  }

}