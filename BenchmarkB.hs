{-|
  BenchmarkB compares a drop-down discrimination mechanism with pattern
  matching (which we expect will be compiled to a jump mechanism).  It
  does so for a very large data type, with eleven constructors.  This 
  allows a comparison of the two different mechanism exposing the
  effect of compiling to a jump mechanism.  The two discrimination 
  mechanisms we compare are @if@ expressions and pattern matching.  If
  expressions work using an equality check on integers so don't need a 
  data type definition.  To compare this to pattern matching we need an 
  equivalent algebraic data type, which we call "Num".  "Num" only includes
  the first ten integers because that is enough to expose the behaviour we
  are trying to capture.
-}
module BenchmarkB where

import Common
import System.Environment
import Data.Array
import Prelude hiding (Num)

-- |An algebraic data type equivalent to the first ten integers.
data Num   = One 
           | Two
           | Three
           | Four
           | Five
           | Six
           | Seven
           | Eight
           | Nine
           | Ten
           | Zero
           deriving Show

pattern POne = One
pattern PTwo = Two
pattern PThree = Three
pattern PFour = Four
pattern PFive = Five
pattern PSix = Six
pattern PSeven = Seven
pattern PEight = Eight
pattern PNine = Nine
pattern PTen = Ten
pattern PZero = Zero

{-|
  Calculate the sum of the longest run of non-zero values in an array of
  integers.  The algorithm uses recursion to iterate through
  the array.
-}
integerRun :: Int    -- ^ The maximim run sum so far seen
           -> Int    -- ^ The sum of the run we are currently in
           -> Int    -- ^ The array element we are inspecting on this call
           -> Array Int Int  -- ^ The array we are inspecting.
           -> Int    -- ^ The sum of the longest run in the array @d@
integerRun maxRun currRun i d = if (i >= snd (bounds d))
                                   then maxRun
                                   else if( d!i == 1) 
                                        then integerRun maxRun (currRun+1) (i+1) d
                                        else if (d!i == 2) 
                                             then integerRun maxRun (currRun+2)  (i+1) d
                                             else if (d!i == 3) 
                                                  then integerRun maxRun (currRun+3)  (i+1) d
                                                  else if (d!i == 4) 
                                                       then integerRun maxRun (currRun+4)  (i+1) d
                                                       else if (d!i == 5) 
                                                            then integerRun maxRun (currRun+5)  (i+1) d
                                                            else if (d!i == 6) 
                                                                 then integerRun maxRun (currRun+6)  (i+1) d
                                                                  else if (d!i == 7) 
                                                                       then integerRun maxRun (currRun+7)  (i+1) d
                                                                       else if (d!i == 8) 
                                                                            then integerRun maxRun (currRun+8)  (i+1) d
                                                                            else if (d!i == 9) 
                                                                                 then integerRun maxRun (currRun+9)  (i+1) d
                                                                                 else if (d!i == 10)
                                                                                      then integerRun maxRun (currRun+10) (i+1) d
                                                                                      else integerRun (max maxRun currRun) 0 (i+1) d
{-|
  Calculate the sum of the longest run of non-zero values in an array of
  "Num"s.  The algorithm uses recursion to iterate through
  the array.
-}
numRun :: Int    -- ^ The maximim run sum so far seen
       -> Int    -- ^ The sum of the run we are currently in
       -> Int    -- ^ The array element we are inspecting on this call
       -> Array Int Num   -- ^ The array we are inspecting.
       -> Int    -- ^ The sum of the longest run in the array @d@
numRun maxRun currRun i d     = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          One   -> numRun maxRun (currRun+1) (i+1) d
                                          Two   -> numRun maxRun (currRun+2) (i+1) d
                                          Three -> numRun maxRun (currRun+3) (i+1) d
                                          Four  -> numRun maxRun (currRun+4) (i+1) d
                                          Five  -> numRun maxRun (currRun+5) (i+1) d
                                          Six   -> numRun maxRun (currRun+6) (i+1) d
                                          Seven -> numRun maxRun (currRun+7) (i+1) d
                                          Eight -> numRun maxRun (currRun+8) (i+1) d
                                          Nine  -> numRun maxRun (currRun+9) (i+1) d
                                          Ten   -> numRun maxRun (currRun+10) (i+1) d
                                          Zero  -> numRun (max maxRun currRun) 0 (i+1) d

synRun maxRun currRun i d     = if (i >= snd (bounds d))
                                   then maxRun
                                   else case d!i of
                                          One   -> synRun maxRun (currRun+1) (i+1) d
                                          Two   -> synRun maxRun (currRun+2) (i+1) d
                                          Three -> synRun maxRun (currRun+3) (i+1) d
                                          Four  -> synRun maxRun (currRun+4) (i+1) d
                                          Five  -> synRun maxRun (currRun+5) (i+1) d
                                          Six   -> synRun maxRun (currRun+6) (i+1) d
                                          Seven -> synRun maxRun (currRun+7) (i+1) d
                                          Eight -> synRun maxRun (currRun+8) (i+1) d
                                          Nine  -> synRun maxRun (currRun+9) (i+1) d
                                          Ten   -> synRun maxRun (currRun+10) (i+1) d
                                          Zero  -> synRun (max maxRun currRun) 0 (i+1) d

{-|
  Compare the run-time of "booleanRun" and "stateRun" on three sets of
  arrays with equivalent values.  One run is with an array of ones and zeros, which
  should show only a little difference between @if@ and @case@.  The second and third runs
  use arrays which are half zero and half 5 and 10 respectively.  We expect
  that the run with 5 will show @if@ taking longer than @case@ and that the run
  with 10 will show the same behaviour but with a larger performance gap.
  This program must be run with two arguments giving the group size and 
  number of groups for the arrays which the benchmark is run for.
-}
main :: IO()
main = do
  args <- getArgs
  let groupSize = read (args!!0) :: Int
  let numGroups = read (args!!1) :: Int
  sequence_ $ map (\(i,n) -> do
                     let integerData = groupsOfX i 0 groupSize numGroups
                     writeFile "/dev/null" (show integerData)
                     runit ("int-"++(show i)) $ integerRun 0 0 0 integerData
                     let numData = groupsOfX n Zero groupSize numGroups
                     writeFile "/dev/null" (show numData)
                     runit ("num-"++(show i)) $ numRun 0 0 0 numData
                     writeFile "/dev/null" (show numData)
                     runit ("syn-"++(show i)) $ synRun 0 0 0 numData
                  )
                  [(1,One),(2,Two),(3,Three),(4,Four),(5,Five),(6,Six),(7,Seven),(8,Eight),(9,Nine),(10,Ten)]